# True Twitter Block

This extension hides the View tweets button on profiles of and the View button for tweets by people you've blocked on Twitter.

## Firefox
https://addons.mozilla.org/en-GB/firefox/addon/true-twitter-block/

## Chrome
https://chrome.google.com/webstore/detail/lenckgkdjkklihheaihglpailohapbcp

## License
The contents of this repository are provided under the GPLv3 license or any later version (SPDX identifier: `GPL-3.0-or-later`).
