const BASE_SEL = 'div[data-testid="primaryColumn"]';

const BLOCKED_TWEET_LABELS = [
	'This Post is from an account you blocked.', // English
	'此貼文來自你已封鎖的帳戶。', // Chinese (traditional)
	'这个帖子来自一个你已屏蔽的账号。', // Chinese (simplified)
	'यह पोस्ट किसी ऐसे खाते से आया है, जिसे आपने अवरुद्ध किया है.', // Hindi
	'Este post es de una cuenta que bloqueaste.', // Spanish
	'Ce post a été publié par un compte que vous avez bloqué.', // French
	'Tento post pochází od účtu, který jste zablokovali.', // Czech
];

const hideViewBlockedTweets = () => {
	let blockedTexts = [];

	for(const el of document.querySelectorAll(`${BASE_SEL} article, #layers article`)) {
		for(const span of el.querySelectorAll('span')) {
			const spanContent = span.innerHTML;

			for(const label of BLOCKED_TWEET_LABELS) {
				if(spanContent === label) {
					blockedTexts.push(span);
					break;
				}
			}
		}
	}

	for(const textEl of blockedTexts) {
		const hideEl = textEl.parentNode.parentNode.parentNode.parentNode.parentNode.querySelector('div:nth-child(2)');
		if(hideEl) {
			hideEl.style.display = 'none';
		}
	}
};

const hideViewProfile = () => {
	if(document.querySelector(`${BASE_SEL} [data-testid$="-unblock"]`)) {
		document.querySelector(`${BASE_SEL} [data-testid="empty_state_button_text"]`).style.display = 'none';
		document.querySelector(`${BASE_SEL} [data-testid="empty_state_body_text"]`).style.display = 'none';
	}
};

const hide = () => {
	hideViewBlockedTweets();
	hideViewProfile();
};

document.addEventListener('load', () => hide());
document.addEventListener('scroll', () => hide());
setInterval(hide, 500);
