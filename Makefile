.PHONY: package-clean package clean

package-clean:
	rm -rf pkg/ true-twitter-block-*.zip

package: package-clean
	cp -r src/ pkg/
	zip -j -r true-twitter-block.zip pkg/
	rm -rf pkg/

clean: package-clean
